﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monitor.Collect.WinService.Tool;
using Monitor.Core;
using Monitor.Domain.Cluster.Dal;
using Monitor.Domain.Cluster.Model;
using Monitor.Domain.PlatformManage.Dal;
using BSF.Db;


namespace Monitor.Collect.WinService
{
    public class CollectVersionUpdateBackgroundTask : BaseBackgroundTask
    {
       
        public override void Start()
        {
            this.TimeSleep = 5000;
            base.Start();
        }
        protected override void Run()
        {
            int maxversion = -1; DateTime sqltime = DateTime.Now;
            SqlHelper.ExcuteSql(CoreGlobalConfig.PlatformManageConnectString, (c) =>
            {
                tb_cluster_collect_version_dal versiondal = new tb_cluster_collect_version_dal();
                maxversion = versiondal.GetMaxVersionNumber(c);
                sqltime = c.GetServerDate();
            });
            if (maxversion > GlobalConfig.VersionNum)
            {
                GlobalConfig.LoadConfig();
                if (GlobalConfig.TaskProvider.IsStart())
                    GlobalConfig.TaskProvider.Stop();
                GlobalConfig.TaskProvider.Start();
                GlobalConfig.VersionNum = maxversion;
                GlobalConfig.ClusterLastUpdateTime = sqltime;
                Core.LogHelper.Log(string.Format("服务器ip【{0}】采集任务dll新版本【" + maxversion+"】更新完毕",GlobalConfig.ServerIP));
            }
           
        }
    }
}
